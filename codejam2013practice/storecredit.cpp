﻿// This is a solution for a CodePlex practice problem (see https://code.google.com/codejam/contest/351101/dashboard#s=p0),
// for a small dataset case:
//Problem
//
//You receive a credit C at a local store and would like to buy two items. You first walk through the store and create a list L of all available items. From this list you would like to buy two items that add up to the entire value of the credit. The solution you provide will consist of the two integers indicating the positions of the items in your list (smaller number first).
//
//Input
//
//The first line of input gives the number of cases, N. N test cases follow. For each test case there will be:
//
//One line containing the value C, the amount of credit you have at the store.
//One line containing the value I, the number of items in the store.
//One line containing a space separated list of I integers. Each integer P indicates the price of an item in the store.
//Each test case will have exactly one solution.
//Output
//
//For each test case, output one line containing "Case #x: " followed by the indices of the two items whose price adds up to the store credit. The lower index should be output first.
//
//Limits
//
//5 ≤ C ≤ 1000
//1 ≤ P ≤ 1000
//
//Small dataset
//
//N = 10
//3 ≤ I ≤ 100
//
//Large dataset
//
//N = 50
//3 ≤ I ≤ 2000
//
//========
//Sample
//========
//Input 
//3
//100
//3
//5 75 25
//200
//7
//150 24 79 50 88 345 3
//8
//8
//2 1 9 4 4 56 90 3
//
//Output 
//Case #1: 2 3
//Case #2: 1 4
//Case #3: 4 5

#include <QtCore/QCoreApplication>
#include <QtCore/qfile.h>
#include <QtCore/qtextstream.h>
#include <iostream>
#include <qstringlist.h>

using namespace std;

typedef struct inputData
{
	quint16 nofTestCases;
	QList<quint16> *pListCredits;
	QList<QList<quint16>* > *pListListPrices;
}InputData;

// Read input data into struct inputData
bool ReadInputData(InputData& inputData)
{
	QFile inputFile("A-small-practice.in");
	if(!inputFile.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		cerr << "Cannot open input file for reading" << endl;
		return false;
	}
	try
	{
		QTextStream in(&inputFile);
		bool intParsingOk = false;
		// read number of testcases
		inputData.nofTestCases = in.readLine().toInt(&intParsingOk);
		if(!intParsingOk || inputData.nofTestCases != 10)
		{
			cerr << "Invalid number of testcases" << endl;
			throw exception();
		}
		// for each testcase
		for(int i = 0; i < inputData.nofTestCases; i++)
		{
			QList<quint16>* pListPricesInStore = new QList<quint16>();

			// read amount of credit for current testcase
			quint16 amountOfCredit = in.readLine().toInt(&intParsingOk);
			if(!intParsingOk || (amountOfCredit < 5 || amountOfCredit > 1000))
			{
				cerr << "Invalid amount of credit" << endl;
				throw exception();
			}
			inputData.pListCredits->append(amountOfCredit);

			// read the list of prices for current testcase
			quint8 nofItems = in.readLine().toInt(&intParsingOk);
			if(!intParsingOk || (nofItems < 3 || nofItems > 100))
			{
				cerr << "Invalid number of items" << endl;
				throw exception();
			}
			QStringList listStrings = in.readLine().split(' ');
			QList<QString> *pListPricesInStoreStrings = dynamic_cast<QList<QString> *>(&listStrings);
			foreach(QString item, *pListPricesInStoreStrings)
			{
				// get current item price
				quint16 itemPrice = item.toInt(&intParsingOk);
				if(!intParsingOk || (itemPrice < 1 || itemPrice > 1000))
				{
					cerr << "Invalid item price" << endl;
					throw exception();
				}
				pListPricesInStore->append(item.toInt(&intParsingOk));
			}
			inputData.pListListPrices->append(pListPricesInStore);
		}
		inputFile.close();
		return true;
	}
	catch(exception const &ex)
	{
		inputFile.close();
		return false;
	}
}

// calculate which pairs of prices in inputData.pListListPrices give exactly the amount defined 
// in the corresponding element of inputData.pListCredits
bool CalculateOutputData(const InputData& inputData, QList<QList<quint8>* >& listOutput)
{
	// for each testcase
	for(quint8 testCaseIndex = 0; testCaseIndex < inputData.nofTestCases; testCaseIndex++)
	{
		QList<quint16> *pListPrices = (*(inputData.pListListPrices))[testCaseIndex];
		quint16 creditAmount = (*(inputData.pListCredits))[testCaseIndex];

		for(quint8 priceIndex = 0; priceIndex < pListPrices->count(); priceIndex++)
		{
			if((*pListPrices)[priceIndex] < creditAmount - 1)
			{
				quint8 finalIndex = 0;
				// we have the potential that the sum of two prices may equal the amount of credit
				for(int secondPriceIndex = priceIndex + 1, finalIndex = secondPriceIndex; secondPriceIndex < pListPrices->count(); secondPriceIndex++)
				{
					if((*pListPrices)[priceIndex] + (*pListPrices)[secondPriceIndex] == creditAmount)
					{
						// we have found a solution
						QList<quint8> listIndexes;
						listIndexes.append(priceIndex);
						listIndexes.append(secondPriceIndex);
						listOutput.append(new QList<quint8>(listIndexes));
						break;
					}
				}
				if(finalIndex == pListPrices->count())
				{
					// error case: no solution was found
					cerr << "No solution found" << endl;
					return false;
				}
			}
		}
	}
	return true;
}

bool WriteOutputData(const QList<QList<quint8>* >& listOutput)
{
	QFile outputFile("A-small-practice.out");
	try
	{
		if(!outputFile.open(QIODevice::WriteOnly)) 
		{
			cerr << "Cannot open output file for writing" << endl;
			throw exception();
		}
		
		QTextStream out(&outputFile);
		for(int i = 0; i < listOutput.count(); i++)
		{
			out << "Case #" << i + 1 << ": " << (*listOutput[i])[0] + 1 << " " << (*listOutput[i])[1] + 1 << endl;
		}
		outputFile.close();
		return true;
	}
	catch(exception const &ex)
	{
		outputFile.close();
		return false;
	}
}

int main(int argc, char *argv[])
{
	InputData inputData = {0};
	inputData.pListCredits = new QList<quint16>();
	inputData.pListListPrices = new QList<QList<quint16>* >();

	if(!ReadInputData(inputData))
	{
		return -1;
	}
	QList<QList<quint8>* > listOutput;
	if(!CalculateOutputData(inputData, listOutput))
	{
		return -1;
	}

	if(!WriteOutputData(listOutput))
	{
		return -1;
	}
	return 0;
}
